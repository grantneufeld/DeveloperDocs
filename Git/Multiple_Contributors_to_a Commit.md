# Including Multiple Contributors for a Single Commit

While there isn’t a built-in way in Git to specify multiple contributors to a single commit, there is a convention that has been adopted by hosts like GitLab and Github.

At the end of a commit message, include one or more “Co-authored-by:” lines, with the contributors name and their email address (one they use on sites like Github and GitLab).

For example:

```git
This is an example commit.

With some detailed explanation of what the commit is for.

Co-authored-by: Some One <someone@example.tld>
Co-authored-by: Anne Other <another@example.tld>
```

---

Source: [Developer Docs](https://gitlab.com/grantneufeld/DeveloperDocs)
