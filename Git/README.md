# Using Git for Version Control / Source Control

## Sub-Articles

* [Including Multiple Contributors for a Single Commit](Multiple_Contributors_to_a Commit.md)
* [Setup Remote Git Repository Using Xcode](Setup_Remote_Git_Repository_Using_Xcode.md)

## Installing Git

On macOS, you can use a system like [Homebrew](https://brew.sh/) to keep Git up to date.

## Making a Commit in Xcode

* ➡️ Integrate → Commit… (⌥-⌘-C)
* 👉 “Stage All” (or select which individual changes to stage, if you don’t want to commit all current changes).
* Set the commit message.
* 👉 “Commit”.

If you have setup a remote repository, you may also want to send the changes to it:

* ➡️ Integrate → Push…

## Commit Messages

Always include a summary of what was changed.

When generating code from a template (e.g., with Xcode’s “New Project”, or “Add Target”), it‘s a good idea to identify the tool that was used (e.g., `Xcode: 15.2`) and options selected for the template.

Basically, any information needed to understand, and replicate, what is done in the commit.

### Active Voice in Commit Messages

It is recommended to use active voice in the text of commit messages.

E.g., “Add Example model” vs. “added example model”.

One way to think of it is that the commit message should answer the question “what would applying this commit do?”

## When to Commit?

In general, the “Single Responsibility Principle” applies to commits: It’s optimal to have a commit be for just one unit of change. E.g., adding a new model, extending an existing model, performing a refactor of a method, updating documentation, etc.

All automated tests should be passing before making a commit. If the project is part of a Continuous Integration and/or Continuous Delivery process, passing tests are a *critical* requirement for all commits.

## Commits in Tutorials Here

At the end of most tutorial steps in this documentation, there is a commit to the source control (version control) repository.

Each commit is marked with a “❇️ Commit Message” heading, followed by a sample commit message appropriate to that step.

---

Source: [Developer Docs](https://gitlab.com/grantneufeld/DeveloperDocs)
