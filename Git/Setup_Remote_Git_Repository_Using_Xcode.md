# Setup Remote Git Repository Using Xcode

## Add a Git Remote to an Xcode Project

With a project open in Xcode:

* Switch to the Source Control Navigator (⌘-2).
* Select the “Repositories” tab.
* Expand the “main” repository (top of the list).
* Right-click/control-click the “Remotes” (bottom of the list).
  - Either create a new, or use an existing, repository.
* I suggest naming the remote based on the service being used (e.g., `gitlab`, `bitbucket`) rather than `origin`.

## Setting Up Multiple Remotes for a Single Project

You can have any number of remotes setup for a project. To add additional remotes after the first, just follow the same steps above for adding a remote.

When pushing in Xcode (Integrate -> Push…), you select which remote to push to. So you’ll need to separately push for each remote in your project.

---

Source: [Developer Docs](https://gitlab.com/grantneufeld/DeveloperDocs)
