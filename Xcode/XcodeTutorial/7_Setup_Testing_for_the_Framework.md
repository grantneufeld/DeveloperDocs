# Setup Testing for the Framework

For this step, we set up a unit test target and test plan for the Framework, assigning the test plan to the Framework scheme.

The unit test target or plan should generally be included among the tests to run for all testable schemes in the project.

---

## Add Framework Unit Tests Target

* ➡️ File → New → Target…
* ➡️ Multiplatform → Test → Unit Testing Bundle
  - Product Name: *Framework Name* + “Tests” (“TutorialFrameworkTests”)
  - Language: Swift
  - Target to be Tested: framework target
  - 👉 “Finish”.
* In the project navigator, move the “*Framework Name*Tests” group into the “Tests” group.

(Ignore any SwiftLint errors that come up, for now.)

Run the tests:

* ➡️ Product → Scheme → select the Framework Tests scheme.
* ➡️ Product → Test (⌘-U)

At this stage, the template tests should pass.

If you see an error like “umbrella header for module 'TuturoialFramework-swift.h' does not include header”, clean the build folder and try again:

* ➡️ Product → Clean Build Folder… (⌘-⇧-K)
* ➡️ Product → Test (⌘-U)

### ❇️ Commit Message

```
Add unit test target for framework.

Xcode: 15.2
Template: Multiplatform: Test: Unit Testing Bundle
```

---

## Cleanup Unit Test File From Template

In the only swift file inside the framework tests group, replace all of the template functions inside the test class with:

    func test_example() throws {
        XCTAssertTrue(true)
    }

You may also want to edit/remove the default header comment.

Make sure the tests are working:

* ➡️ Product → Test (⌘-U)

### ❇️ Commit Message

```
Cleanup template test code.
```

---

## Create and Configure the Framework Test Plan

Create the Framework test plan:

* ➡️ Product → Scheme → make sure the TutorialFramework scheme is selected.
* ➡️ Product → Scheme → Edit Scheme… (⌘-< or ⌘-⇧-,)
* Select the “Test” section on the left.
* 👉 the small arrow button on the right side of the test plan (“TutorialFramework (Autocreated)”).
* ➡️ File → Save… (⌘-S)
  - The name given in “Save As:” should be good (“TutorialFramework.xctestplan”), although you could name it whatever you want (perhaps a shorter “Framework.xctestplan”).
  - Navigate inside the Tests directory
  - Group: Tests
  - 👉 “Save”.

* Optionally, in the Project Navigator (⌘-1), move the new test plan to the bottom of the Tests group. (I generally keep all test plans at the end of the Tests group.)

Add the framework unit tests to the the test plan:

* Select the framework test plan in the Project Navigator (⌘-1) (should already be selected from the previous step).
* Make sure the “Tests” tab is selected in the test plan editor pane.
* 👉 the “+” at the bottom left of the test plan editor pane.
  - Select the framework tests target.
  - 👉 “Add”.
* 👉 “Options…” (to the right of the framework tests target in the list).
  - Execute in parallel: On
* Select the Configurations tab.
* Make sure “Shared Settings” is selected.
* Test Execution: Execution Order: Random
* Code Coverage: Code Coverage: On
  - Gather coverage for: some targets
  - 👉 “+”
    - select the framework target.
  - Tap away from the pop-up selector to close it.

Run the tests:

* ➡️ Product → Test (⌘-U)

The tests should be passing at this stage.

### ❇️ Commit Message

```
Create the framework test plan.

The test plan includes the framework unit tests.
```

---

## Remove the Unit Tests Scheme

When the framework unit tests target was created, the template included a scheme for the unit tests. Since we include the unit tests in the test plan for the framework scheme, we no longer need the separate unit tests scheme.

* ➡️ Product → Scheme → Manage Schemes…
* Select the “TutorialFrameworkTests” scheme.
* 👉 the “-” button near the bottom left of the window.
  - Confirm the deletion by tapping 👉 “Delete”.

Run the tests:

* ➡️ Product → Test (⌘-U)

The tests should be passing at this stage.

(There’s no commit message, because Xcode was managing the scheme file “behind the scenes”, so this change doesn’t affect the files in our project directory.)

---

Source: [Developer Docs](https://gitlab.com/grantneufeld/DeveloperDocs)
