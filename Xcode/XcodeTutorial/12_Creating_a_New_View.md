# Creating a New View

## Remove the Template Unit Tests file

The template example UI tests file will longer be needed need, as we’re creating our own tests now.

* In the Project Navigator (⌘-1), select the `TutorialUITests.swift` file in the `TutorialUITests` group.
* ➡️ Edit → Delete (⌫)
  - 👉 “Move to Trash”.

## TDD: Create a UI Test First

Since this is the first UI test being added to the project,

Create a UI tests file for the new view:

* ➡️ Product → Scheme → select the App scheme.
* In the Project Navigator (⌘-1), select the App UI Tests group.
* ➡️ File → New → New File… (⌘-N)
  - macOS: Source: UI Test Case Class
* 👉 Next.
  - Class: TitleViewUITests
* 👉 Next.
  - Make sure the App UI Tests folder is selected.
  - Group: App UI Tests
  - Targets: App UI Tests
* 👉 Create.

Reduce the template code in the `TitleViewUITests` class to:

```swift
override func setUpWithError() throws {
    continueAfterFailure = false
}
```

Add a test for the (not yet created) new view:

```swift
func test_contentView_containsTitleView() throws {
    let app = XCUIApplication()
    app.launch()
    let title = app.staticTexts["title"]
    XCTAssertTrue(title.exists)
}
```

Run the new test, either by tapping the diamond beside the test, or ➡️ Product → Test (⌘-U).

The test will fail (“XCTAssertTrue failed”), because there’s no view with the “title” id.

Create the view:

* In the Project Navigator, select the App group (“TutorialApp”).
* ➡️ File → New → File… (⌘-N)
  - macOS: User Interface: SwiftUI View
* 👉 Next.
  - Save As: TitleView
  - Make sure the App group (“TutorialApp”) is selected.
  - Group: the App group (“TutorialApp”)
  - Targets: the App target (“TutorialApp”)
* 👉 Create.

Reposition the new TitleView file in the Project Navigator. I suggest moving it to immediately after the ContentView.

Change the `body` property of the new `TitleView` struct to:

```swift
var body: some View {
    Text("a")
        .accessibilityIdentifier("title")
}
```

Open the `ContentView` file (in the App group in the Project Navigator).

Replace the `body` property of the `ContentView` struct with:

```swift
var body: some View {
    TitleView()
}
```

Rerun the `test_contentView_containsTitleView` test, either by tapping the diamond beside the test, or ➡️ Product → Test (⌘-U).

The test should now be passing.

Back in the TitleViewUITests, add another test function:

```swift
func test_titleView_hasExpectedText() throws {
    let app = XCUIApplication()
    app.launch()
    let title = app.staticTexts["title"]
    let titleValue = try XCTUnwrap(title.value as? String)
    XCTAssertEqual(titleValue, "Tutorial Title!")
}
```

Run the new `test_titleView_hasExpectedText` test, either by tapping the diamond beside the test, or ➡️ Product → Test (⌘-U).

The test will fail (“XCTAssertEqual failed: ("a") is not equal to ("Tutorial Title!")”) because the title text is just `"a"`, instead of the `"Tutorial Title!"` the test expects.

Back in the `TitleView` struct, change the `Text` parameter from `"a"` to `"Tutorial Title!"`.

Rerun the `test_titleView_hasExpectedText` test, either by tapping the diamond beside the test, or ➡️ Product → Test (⌘-U).

The test should now be passing.

### ❇️ Commit Message

```
Add TitleView SwiftUI View.

Xcode: 15.2
Template: macOS: Source: UI Test Case Class
Template: macOS: User Interface: SwiftUI View
```

---

Source: [Developer Docs](https://gitlab.com/grantneufeld/DeveloperDocs)
