# The Empty Project Template

Create an empty project in Xcode, then initialize a Git source control repository for it.

## Why start from an “Empty” project template?

Xcode’s other project templates (such as the App templates) are usually quite convenient, and work well enough for many uses.

However, there can be some hidden complications with the templates.

For example, with the app templates, the project name gets used as the name for the app target and group, and, similarly, for the test templates the name is used in names and paths if “include tests” was selected. This can cause problems when trying to rename things (groups, targets) because the name may be depended upon by various configuration settings in the project that aren’t automatically updated.

Further, starting from an Empty project allows us to be very specific in how we structure the project, rather than following the defaults in the App (or other) templates.

It is possible to setup custom project (and file) templates for Xcode to use, so if you have a preferred project structure you could setup a template for that which you could use instead of following the process here.

## Why turn off auto-creation of the git repository for the project?

Mainly because I find the commit message that Xcode assigns to the first commit to be inadequate. (You might still use auto-create, if you’re okay with that message, or can amend the commit to have a better message.)

---

## Creating a new project using the Empty template

* Open Xcode

Create the new empty project:

* ➡️ File → New → Project… (⌘-⇧-N)
  - Other: Empty
  - 👉 “Next”.
* configure the project
  - Product Name: *Project Name* (e.g., “Tutorial”)
* save project
  - Source Control: off
  - 👉 “Create”.

* Close the new project

Initialize the git repository:

* Switch to Finder
* Navigate to the new project folder

* Open Terminal
  (you can drag the project folder onto the Terminal window to enter the path)
  ```sh
  cd 'path'
  git init
  git add .
  ```

* Re-open the project in Xcode.

## ❇️ Commit Message

```
Initial empty Xcode project.

Xcode: 15.2
Template: Other: Empty
```

---

## Optional: Setup Remote Repository

If you want to have your Git source repository for the project mirrored to a remote host, like GitLab or Github, this would be the point to set that up.

* [Setup Remote Git Repository Using Xcode](../../Git/Setup_Remote_Git_Repository_Using_Xcode.md).

---

Source: [Developer Docs](https://gitlab.com/grantneufeld/DeveloperDocs)
