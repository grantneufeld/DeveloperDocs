# Setup UI Tests

For this step, we set up an UI Testing Bundle target for the project, and add it to the app scheme..

---

## Add App UI Tests Target

* ➡️ File → New → Target…
  - Multiplatform: Test: UI Testing Bundle
  - 👉 “Next”.
  - Product Name: *App Target Name* + “UITests” (“TutorialUITests”)
  - Language: Swift
  - Target to be Tested: App target (“Tutorial”)

* Open the Project Navigator (⌘-1).
* Move the UI tests group into the Tests group, above the test plans.

At this point, we’re unable to run the new UI tests, because they are not part of our test plans. But, that’s something to fix in the next step.

### ❇️ Commit Message

```
Add App UI Tests Target.

Xcode: 15.2
Template: Multiplatform: Test: UI Testing Bundle
Target to be Tested: Tutorial
```

---

## Add the UI Tests to the App test plan.

In general, it’s good to start off with the UI tests as part of the app test plan.

But, if you find some of the tests are slow, you can move them into another UI tests target (e.g., “TutorialAppSlowUITests”) that is not included with the app test plan, leaving only the sufficiently fast UI tests to be run when in the app scheme.

* In the Project Navigator (⌘-1), select the app test plan inside the Tests group.
* Make sure the Tests tab is active in the test plan editor pane.
* 👉 “+”, at the bottom left of the test plan editor pane.
  - Select the UI tests target.
  - 👉 “Add”.
* 👉 “Options…”, for the UI tests.
  - Execute in parallel: on

Run the tests:

* ➡️ Product → Scheme → make sure the app scheme is selected.
* ➡️ Product → Test (⌘-U)

All of the template tests should pass.

(There will be some SwiftLint warnings that you can ignore for now. They’ll be cleaned up in the next section.)

### ❇️ Commit Message

```
Add the UI tests to the app test plan.
```

---

## Cleanup UI Tests Template Code

Remove unwanted example tests.

The launch performance tests and launch configuration tests tend to be time consuming, and are only really useful if they are set up deliberately. So, we’ll remove them from this project (for now). If needed later, a new test target could be set up, to isolate them from the regular UI tests.

Get rid of the UI “LaunchTests” file:

* In the Project Navigator (⌘-1), select the “…LaunchTests” file in the UI tests group.
* ➡️ Edit → Delete (⌫)
  - 👉 “Move to Trash”.

In the remaining UITests file:

* Remove the `testLaunchPerformance` function, since we’re not dealing with speed checks at this point.
* Remove the `tearDownWithError` function, since it’s empty.
* Optional: Remove the template comment lines.
* Reduce the content of the `testExample` function to just: `XCTAssertTrue(true)`

Confirm that everything is still working:

* ➡️ Product → Clean Build Folder… (⌘-⇧-K)
  - 👉 “Clean”.
* ➡️ Product → Scheme → make sure the app scheme is selected
* ➡️ Product → Test (⌘-U)

The tests should be passing, and running much faster now that the launch and configuration tests have been removed. This will have also removed the SwiftLint warnings.

### ❇️ Commit Message

```
Cleanup UI tests template code.
```

---

Source: [Developer Docs](https://gitlab.com/grantneufeld/DeveloperDocs)
