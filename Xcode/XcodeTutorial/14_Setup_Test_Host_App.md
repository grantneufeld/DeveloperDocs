# Setup a Test Host App

*Optional.*

Most of the time, running the framework unit tests without a host app works just fine.

However, if your tests are performing “sandboxed” actions (such as reading files outside the build directory), you will need a host app, with the appropriate entitlement(s) set.

You could use an existing app target in the project, but it would need the entitlement(s) your tests require. More importantly, it’s best to avoid using a full app for a project’s unit tests (because it can slow down the tests, as well as performing launch activities that may not be appropriate to do as frequently as tests can be run).

## Add a Test Host Target

Following the practice of clear names, name this target “***TestHostApp***”.

🛑 ...

* Open the project in the Project Navigator.
* Add Target
  - macOS: Application: App
  - Tap “Next”
  - Product Name: TestHostApp
  - Interface: SwiftUI (doesn’t really matter, since the app will be stripped down, anyway)
  - Language: Swift
  - Storage: None
  - Include Tests: off
  - Tap “Finish”


### ❇️ Commit Message

```
Add TestHostApp target.

Xcode: 15.2
Template: macOS: Application: App
Interface: SwiftUI
Language: Swift
Storage: None
Include Tests: off
```

---

## Move TestHostApp Group into Tests Group

> ⚠️ WARNING: Xcode is somewhat _fragile_ when trying to relocate a group
> for an app target. Just moving the group into a sub-group in the project
> navigator can result in Xcode crashing and leaving the project in a messy
> state, where it fails to recognize where the new location is.

So, instead, we’ll take this complicated approach:

* In the Project Navigator.
* Select the TestHostApp group (folder).
* Edit -> Delete (command-delete)
  - Tap “Remove References”

In the Finder:

* Move the TestHostApp folder into the Tests folder.

Back in Xcode:

* Drag the TestHostApp folder (which is now inside the Tests folder)
  from the Finder into the Tests group in the Xcode Project Navigator.
  - Destination: Copy items if needed: off
  - Added folders: Create groups
  - Add to targets: TestHostApp
  - Tap “Finish”
* Position the TestHostApp group where you want in the Tests group
  (I usually put it near the end, just before the test plans).

### Update Paths in Build Settings

There are some references to the location of files in the TestHostApp group that need to be updated, now that it has moved.

* In the Project Navigator, open the project (top of the list)
* Select the TestHostApp target.
* Switch to the Build Settings tab
* Update the following paths, replacing the references to `TestHostApp/` with `Tests/TestHostApp/`:
  - Deployment: Development Assets
  - Signing: Code Signing Entitlements

### Confirm that the app can build and run

* Make sure the TestHostApp scheme is selected.
* Product -> Run (command-R)

### ❇️ Commit Message

```
Move TestHostApp group into Tests group.
```

---

## Setup the Test Host Entitlements

You may want to be more specific in which entitlements you assign to your
test host, but a reasonable general setup is to just turn off the sandbox
for the Test Host, since the host app is only being used for unit testing.

* In the Project Navigator, select the TestHost entitlements file in the TestHost group. (Yellow, rectangular, “certificate” icon).
* Edit the key/value pairs:
  - App Sandbox: NO
  - (Optional) Delete the “Access User Selected Files (Read Only)” setting.

After changing entitlements, the app should always be rebuilt from scratch.

* Product -> Clean Build Folder… (shift-command-K)

Confirm that the app can still run:

* Product -> Run (command-R)

### ❇️ Commit Message

```
Set TestHost app entitlements to turn off App Sandbox.
```

---

## Trim the App Code

Edit TestHostApp/TestHostApp.swift:

* Rename `TestHostAppApp.swift` to `TestHostApp.swift`.
* Rename the `TestHostAppApp` struct to just `TestHostApp`.

Edit TestHostApp/ContentView.swift:

* Replace the cotent of the `body` property with just `Text("Test Host")`.

Confirm that the app can still run:

* Product -> Run (command-R)

### ❇️ Commit Message

```
Trim the TestHostApp code.
```

---

## Remove the TestHostApp Scheme

* Product -> Scheme -> Manage Schemes…
* Select the TestHostApp scheme.
* Tap the “-” near the bottom left.
  - Tap “Delete”

### ❇️ Commit Message

```
Remove the TestHostApp scheme.
```

---

## Add the TestHostApp as Host for Tests

* Open the project in the Project Navigator.
* Select the tests target.
* Select the “General” tab.
* Testing: Host Application: TestHostApp
  - Allow testing Host Application APIs: off

Confirm the tests can run:

* Make sure the appropriate scheme is active.
* Product -> Test

### ❇️ Commit Message

```
Add the TestHostApp as host application for tests.
```

---

Source: [Developer Docs](https://gitlab.com/grantneufeld/DeveloperDocs)
