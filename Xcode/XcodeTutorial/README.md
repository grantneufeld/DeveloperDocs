# Xcode Project Structure Tutorial

This is a multi-part tutorial, covering the structure of Xcode projects, especially the roles of:

* Targets
* Test Plans
* Schemes

For the tutorial, we’ll be setting up a multi-platform app (iOS & macOS) using SwiftUI. However, much of the process covered here can be applied to any type of Xcode project.

## About This Tutorial

It will help to be familiar with the basic [Xcode Project Terminology](../Terminology.md).

### Assumptions

You have:

* General familiarity with the Swift programming language.
* Previously used Xcode.
* Basic familiarity with [using Git for source control](../../Git/).

### Legend

“➡️” is used to identify a menu command.<br>
“👉” is used to identify a button to tap.

### Commits

Each commit step in these tutorials is marked with a “❇️ Commit Message” heading, followed by a sample commit message appropriate to that step.

Once the “TestEverything” scheme has been setup for a project, it’s useful to run the “TestEverything” tests before every commit.

## Tutorial Chapters

### General Project Setup

1. [Choosing Names Before Starting a New Xcode Project](1_Choosing_Names_Before_Starting_a_New_Xcode_Project.md)
2. [The Empty Project Template](2_The_Empty_Project_Template.md)
3. [Basic Project Documentation](3_Basic_Project_Documentation.md)

### Setting Up the App & Testing Structures

4. [Structure of Automated Tests in an Xcode Project](4_Structure_of_Automated_Tests_in_an_Xcode_Project.md)
5. [Using a Framework for Core Code](5_Using_a_Framework_for_Core_Code.md)
6. [Setup SwiftLint](6_Setup_SwiftLint.md)
7. [Setup Testing for the Framework](7_Setup_Testing_for_the_Framework.md)
8. [Setup App Target](8_Setup_App_Target.md)
9. [Setup UI Tests](9_Setup_UI_Tests.md)
10. [Setup to Test Everything](10_Setup_to_Test_Everything.md)

### Writing Code

11. [Creating a New Model](11_Creating_a_New_Model.md)
12. [Creating a New View](12_Creating_a_New_View.md)

### Other Setups (optional)

13. [Using Multiple Test Plans for a Scheme](13_Using_Multiple_Test_Plans_for_a_Scheme.md)
14. [Setup a Test Host App](14_Setup_Test_Host_App.md)

## Troubleshooting

Refer to [Troubleshooting Xcode Errors](Troubleshooting_Xcode_Errors.md)
for steps to take, if errors are encountered while following the steps in this tutorial.

---

Source: [Developer Docs](https://gitlab.com/grantneufeld/DeveloperDocs)
