# Choosing Names Before Starting a New Xcode Project

There are some decisions best made **before** creating a new Xcode project.

## Important names for a project

The rest of this tutorial references these names:

* ***Project Name*** (e.g., `Tutorial`)
* ***Framework Name*** (e.g., `TutorialFramework`)
* ***App Display Name*** (e.g., `Tutorial`—does not have to match the project name)
* ***App Target Name*** (e.g., *App Display Name* + `App` → `TutorialApp`)
* ***TestEverything*** (for the scheme & test plan that will run every test)

If in doubt as to what your app/product will be called, you could use generic names for the framework and app targets: `Framework`, `App` (or platform specific, such as `iOSApp`, `macOSApp`, `SwiftUIApp`, …).

### Why avoid renaming?

**⚠️** Changing the names of targets and their root groups (directories) in a project can have complications.

Depending on the Xcode templates used when creating projects and targets, the names may be used in a number of configuration settings and paths. Xcode does not provide a comprehensive renaming function/interface, so if you do change one of those names, you may need to dig through the project to update any setting that uses the old name (a sometimes frustrating, confusing, or painful, process).

---

Source: [Developer Docs](https://gitlab.com/grantneufeld/DeveloperDocs)
