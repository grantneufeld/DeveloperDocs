# Basic Project Documentation

Even for private personal projects that only you will ever see, a README is a reference point that future-you will be able to use to remember what the purpose and setup of the project is.

## Add a README

* ➡️ File → New → File… (⌘-N)
  - Choose the “Markdown File” template (It doesn’t matter which platform is selected).
  - Save As: “README”
  - Group: The top-level project
  - Targets: (none should be selected)
  - Save in the root directory of the project.

Edit the new file as appropriate to your project.

```md
# Xcode Setup Tutorial

A cross-platform SwiftUI app project to document my current understanding
of setting up Xcode projects.

## Project Structure

Uses a Framework Target within the project for the core/shared code,
that can be unit tested efficiently, and an App Target for UI and other
App details. 

Divides tests into 3 (so far) targets:

* Unit tests (targetting the framework).
* App UI tests.
* App UI Configuration Tests.

The project has 3 (so far) Scheme & Test Plan pairs:

* The framework target Scheme, with framework tests Test Plan
  (just the unit tests).
* The app target Scheme, with app tests Test Plan
  (also just the unit tests, for now).
* A “TestEverything” Scheme, with a “TestEverything” Test Plan
  (which includes all of the test targets).

## Tools & Libraries

* Xcode: 15.2
* [SwiftLint](https://github.com/realm/SwiftLint): 0.57.0

## Legal

Copyright ©2024 by Grant Neufeld. All Rights Reserved.
```

It’s good to include:

* Project name.
* Summary description of what the project is about (including what sort of thing(s) it will build).
* A list of tools & libraries used, including versions. Preferably with weblinks.
* Installation instructions.
* If the project is going to be shared with others, a weblink(s) to the project (website, primary source repository, etc.).
* Author(s) contact information.
* Legal notices, if applicable.

If you are intending the README for developers who may work on the project, it can also be good to include an explanation of the project structure (both code and tests). Although, for more complex projects, you may instead want a dedicated documentation folder with multiple documents to cover the various aspects of the project.

## ❇️ Commit Message

```
Add README.

Xcode: 15.2
Template: macOS: Other: Markdown File
```

## Making the Project Source Public

If you have decided to make a project available for use as Open Source (and similar forms of public code release), you will need to choose one or more licenses.

[Choose a License](https://choosealicense.com/) can help with choosing an open source license.

### Accepting Contributions

If the project is public (e.g., open source), and will be open to additional contributors, it will need a code of conduct (`CODE_OF_CONDUCT.md`), and a guide to contributing (`CONTRIBUTING.md`).

[Contributing-Gen generator](https://generator.contributing.md/) can generate detailed templates for those documents.

---

Source: [Developer Docs](https://gitlab.com/grantneufeld/DeveloperDocs)
