# Using Multiple Test Plans for a Scheme

***Optional***.

When working on an app target, you may want to have a faster set of tests that you can run frequently while working on the code. But there may also be a set of slower tests you would want to run occasionally for that target, without having to run the `TestEverything` scheme.

## Setup a Test Plan for Slower App Tests

Create a new test plan:

* ➡️ Product → Scheme → select the scheme you want multiple test plans for
* ➡️ Product → Scheme → Edit Scheme… (⌘-< or ⌘-⇧-,)
* Select the “Test” section in the left column.
* 👉 “+”, near the bottom left of the Test Plans list.
  - Create empty Test Plan…
    - Save As: *App Target Name* + “SlowTests” (or something like that)
    - Make sure the Tests directory is selected.
    - Group: Tests
    - 👉 “Save”.

Add the applicable test targets to the new test plan:

* 👉 the small arrow to the right of the new test plan name to open the test plan editor.
* 👉 “+” at the bottom-left of the test plan editor pane.
  - Select the test targets you want to include for the app slow tests.
  - 👉 “Add”.
* For each test target, 👉 “Options…”, and turn on Execute in parallel.

### Switching Between Test Plans for a Scheme

While the scheme is active, you can switch between its available test plans:

* ➡️ Product → Test Plan → select the test plan

Note that the scheme will return to the test plan marked as “Default” whenever the project is reloaded, but not when just switching between schemes.

### Testing the New Test Plan

With the new test plan selected for the scheme, run the tests:

* ➡️ Product → Test (⌘-U)

The test run should be successful at this point.

### ❇️ Commit Message

```
Add a new test plan for slow app tests to the app scheme.
```

---

Source: [Developer Docs](https://gitlab.com/grantneufeld/DeveloperDocs)
