# Creating a New Model

When adding code, unless it is specifically for the application UI, it should generally be added to the framework target.

As the purpose of this tutorial is to bring us toward a good standard of project quality, the best practice of Test-Driven Development (TDD) is used here.

We’ll create an `Example` model with one property, `value: Int`, and a `sum()` function to get the sum of the values of two examples.

## Remove the Template Unit Tests file

The template example unit tests file will longer be needed need, as we’re creating our own tests now.

* In the Project Navigator (⌘-1), select the `TutorialFrameworkTests.swift` file in the `TutorialFrameworkTests` group.
* ➡️ Edit → Delete (⌫)
  - 👉 “Move to Trash”.

## TDD: Create a Unit Test First

Make sure the active scheme is the framework (not the app, or the TestEverything, schemes).

Since this is the first model being added to the project, add a “Models” group in the framework tests:

* In the Project Navigator (⌘-1), select the framework tests group.
* ➡️ File → New → Group (⌥-⌘-N)
* Set the name of the new group to “Models”

Create a new unit test file:

* With the Models group still selected:
* ➡️ File → New → File… (⌘-N)
  - macOS: Source: Unit Test Case Class
* 👉 “Next”.
  - Class: ExampleTests
  - Subclass of: XCTestCase (should be set by default)
  - Language: Swift
* 👉 “Next”.
  - Make sure that the Models directory is selected.
  - Targets: just the framework tests target.
* 👉 “Create”.

Edit the test file:

* Add `import TutorialFramework` at the top of the file.
* Delete all of the functions in the template.
* Add the following test function, which will report an error that it “Cannot find 'Example' in scope”.

```swift
func test_sum_givenBothAreZero_returnsZero() {
    let sut = Example()
}
```

Following TDD, that error tells us the next thing we need to do: Add an `Example` class.

First, add a “Models” group in the framework group:

* In the Project Navigator (⌘-1), select the framework group.
* ➡️ File → New → Group (⌥-⌘-N)
* Set the name of the new group to “Models”

Create a new Swift file:

* With the framework Models group still selected:
* ➡️ File → New → File… (⌘-N)
  - macOS: Source: Swift File
* 👉 Next.
  - Save As: Example
  - Make sure that the Models directory is selected.
  - Targets: just the framework target.
* 👉 Create.

Set the content of the new Example.swift file to:

```swift
public final class Example {
    public init() {}
}
```

(Ignore any SwiftLint warnings, for now.)

> Note that one key difference in having code in a Framework, rather than directly in the app target, is that things have to be declared `public` if they are to be visible outside the framework (i.e., by the app UI code).

Back in the `ExampleTests`, add `0` as a a parameter to the initializer: `let sut = Example(0)`.

In `Example.swift`, change the initializer to `public init(_ value: Int) {}`.

In `ExampleTests`, change the test code to:

```swift
let sut = Example(0)
let other = Example(0)
let result = sut.sum(other)
```

In `Example.swift`, add the sum function:

```swift
public func sum(_ other: Example) -> Int {
    -1
}
```

In `ExampleTests`, add an assertion to the end of the test: `XCTAssertEqual(result, 0)`.

Run the tests:

* ➡️ Product → Test (⌘-U)

This should produce an error (`XCTAssertEqual failed: ("-1") is not equal to ("0")`).

Back in `Example.swift`, change the `-1` return value to zero `0`.

Run the tests again, and they should pass.

* ➡️ Product → Test (⌘-U)

Add one more test to `ExampleTests`:

```swift
func test_sum_givenValuesOfOneAndTwo_returnsFive() {
    let sut = Example(1)
    let other = Example(2)
    let result = sut.sum(other)
    XCTAssertEqual(result, 3)
}
```

Run the tests:

* ➡️ Product → Test (⌘-U)

This should produce an error (`XCTAssertEqual failed: ("0") is not equal to ("3")`).

Change the code of the `Example` file to, including DocC formatted documentation comments:

```swift
/// An example model for use in a tutorial.
public final class Example {
    /// A numeric value that can be part of a sum.
    private let value: Int

    /// - Parameter value: A numeric value that can be part of a sum.
    public init(_ value: Int) {
        self.value = value
    }

    /// Produce the sum of the values of two examples.
    /// - Parameter other: An `Example` to get a value from for the sum.
    /// - Returns: The sum of the object’s value and the passed in example’s value.
    public func sum(_ other: Example) -> Int {
        self.value + other.value
    }
}
```

Run the tests again, and they should pass.

* ➡️ Product → Test (⌘-U)

### ❇️ Commit Message

```
Add Example model.

Xcode: 15.2
Template: macOS: Source: Unit Test Case Class
Template: macOS: Source: Swift File
```

---

Source: [Developer Docs](https://gitlab.com/grantneufeld/DeveloperDocs)

