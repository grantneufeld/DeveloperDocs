# Structure of Automated Tests in an Xcode Project

## Test Targets

Normally, there will be one or more “targets” for tests in a project. When a project is created using Xcode’s app template, with the “Include tests” option selected, there will be 2 test targets: one for unit tests, and one for UI tests.

It is possible to set up any number of test targets in a project.

## Test Plans

Xcode uses “test plans”, which define a set of tests (based on targets) that can be run, and configuration for how to run those tests. These are saved as `.testplan` files.

You may not have encountered test plans, even if you’ve been working with tests in Xcode projects. If no test plan file is explicitly defined for a project, Xcode manages defaults behind the scenes.

You can have multiple test plans for a project, allowing for defining sets of tests to run under different contexts, and for running the same tests with different configurations.

## Categorizing Tests

Test targets can be roughly categorized as fast, slow, or very slow. Well written unit tests are typically very fast, while things like UI tests can take a long time on a larger project.

Generally, it is useful to have the fast tests run very frequently while working on a project, but only run the slower tests occasionally—such as before making a commit, or when specifically relevant such as when working on UI code.

Things like mutation testing are so slow that it is generally best to setup for running them separately, such as on a dedicated build server or on a workstation that is not going to be needed for a long time (such as overnight after work).

### Fast Tests

* Unit Tests.
* Integration Tests, sometimes.
* Approval Tests (usually).

### Slow Tests

* UI Tests.
* Integration Tests, sometimes.
* Acceptance Tests (e.g., BDD, Gherkin)

### Very Slow Tests

* Profiling.
* Fuzz testing (usually).
* Mutation testing.

## Dividing up the tests

The unit tests for the core code of the app (the shared framework in this tutorial) should generally be included in every test plan.

For when working on the framework code, we will have:

* A `Framework` scheme and test plan that includes:
  - Just the framework unit tests target.

To separate out the slower tests, but still make them available to run,
we can make one or more schemes and test plans that include some or all of them.

* An “App Tests” per app target: A scheme that can build, run, and test,
  a specific app target (e.g., `iOSAppTests`).
  The scheme to have active when working on app-specific (typically UI) code.
  The accompanying test plan includes:
  - App-target-specific unit tests (if any).
  - App-target-specific integration tests (if any, and they’re not too slow).
  - App UI tests for the app target (if they’re not too slow).
  - Framework unit tests.
* `TestEverything` (you can name it something else):
  A scheme and test plan that includes all test targets.
  Some test targets might only be included in this scheme & test plan:
  - Slow Integration or UI Tests.
  - Acceptance/Behaviour Tests (BDD).
    These are not used in most Xcode projects. But, in case the project does include
    acceptance tests, a separate target should be added for them, like with slow
    integration tests. Or, possibly a target that includes both the integration
    and acceptance tests.
  - Application UI Configurations.
    This is a type of UI test that runs the same test through each type of
    UI configuration (dark/light modes, etc.).

You may find that some of the App UI tests become burdensome to include in the app scheme & test plan. You could remove the UI tests target from the app scheme & test plan (leaving it to run only as part of `TestEverything`), or create another UI Test target for the slow tests and add that to `TestEverything` (leaving only the “fast enough” UI tests in the app scheme & test plan).

---

Source: [Developer Docs](https://gitlab.com/grantneufeld/DeveloperDocs)
