# Using a Framework for Core Code

For this step, we set up an Framework target for the project.

We use a “Framework” within the project to hold all of the code that is not tied to the app interface (e.g., not SwiftUI, command line tool interface, AppKit, UIKit, etc.).

A framework has the advantage of being testable without needing to load an app (such as through the Simulator or on a device destination). This can make for (sometimes much) faster unit test runs.

This also makes it easier to share common code if you decide to add additional build destinations (such as having a SwiftUI app and a command line tool).

## Add Framework Target

* ➡️ File → New → Target…
  - Multiplatform: Framework & Library: Framework
  - 👉 “Next”.
  - Product Name: *Framework Name* (“TutorialFramework”)
  - Include Tests: Off
  - Include Documentation: Off
  - Embed in Application: None
  - 👉 “Finish”.

That will create a new group/directory in the project, containing a header file (`TutorialFramework.h`). You can safely ignore that file unless you need to make your framework available to call from Objective-C code.

Set the product destination to the computer being worked on (rather than a simulator, or connecte device):

* ➡️ Product → Destination → My Mac

Confirm that the framework can build:

* ➡️ Product → Build (⌘-B)

### ❇️ Commit Message

```
Add Framework Target: TutorialFramework.

Xcode: 15.2
Template: Multiplatform: Framework & Library: Framework
Include Tests: Off
Include Documentation: Off
Embed in Application: None
```

---

## Make Xcode create the default scheme file

Xcode keeps an auto-generated scheme for the target, until a change is made to the scheme that deviates from the defaults.

This can be surprising, or even annoying, when working on something else and a scheme file (file extension: `.xcscheme`) suddenly appears in the project.

The following little bit of trickery should cause Xcode to create the scheme file, even though we haven’t changed any of the default configuration for it:

* ➡️ Product → Scheme -> Manage Schemes…
  - Uncheck, and then recheck, the “Shared” checkbox on the right side of the “TutorialFramework” line.
  - 👉 “Close”.

### ❇️ Commit Message

```
Make Xcode create the default file for the framework scheme.

Xcode: 15.2
```

---

## Configure The Overall Project

Now that there is a target, some settings become available for the project.

* In the Project Navigator (⌘-1), select the project at the top of the list of files.
* In the Project editor, select the project (on the left side, above the targets).
  - Info: Deployment Target: Set the minimum OS versions you want to require for the project. For this tutorial, we’ll set it to the highest version listed.

### ❇️ Commit Message

```
Configure deployment targets for project.
```

---

## Configure Framework Target

* In the Project editor, select the framework target (it’s currently the only target).
  - General: Supported Destinations: Add or remove destinations as appropriate.
  - General: Minimum Deployments: Make sure these are the same as you set for the overall projet settings.

### ❇️ Commit Message

```
Configure framework.
```

---

Source: [Developer Docs](https://gitlab.com/grantneufeld/DeveloperDocs)
