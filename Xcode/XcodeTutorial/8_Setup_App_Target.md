# Setup App Target

For this step, we set up an App target for the project, and add the framework as a dependency.

---

## Add App Target

Here, we’re adding a multiplatform (iOS & macOS) app target to the project. If you are building something different (for a different platform, just one platform, etc.,…), you would add a target for that, instead.

* ➡️ File → New → Target…
  - Multiplatform: Application: App
  - Product Name: *App Target Name* (“Tutorial”)
  - Storage: None
  - Include Tests: off

You can also move the new “Tutorial” group to a better position in the Project Navigator.

I prefer to arrange my targets so that all of the tests are at the end of the list. (This is not required.)

Reposition the new target in the project editor:

* Open the Project Navigator (⌘-1).
* Open the project (top of the list in the Project navigator).
* Move the app target to right after the framework in the list.

Check that the new app target can build and run:

* ➡️ Product → Scheme → select the new app scheme
* ➡️ Product → Run (⌘-R)

(You can safely quit the Tutorial app after it runs.)

Xcode will have probably opened the debug-area panel when the app was run. Close it for now:

* ➡️ View → Debug Area → Hide Debug Area (⌘-⇧-Y)


### ❇️ Commit Message

```
Add an App target template.

Xcode: 15.2
Template: Multiplatform: Application: App template.
Storage: None
Include Tests: off
```

---

## Create the App Scheme File

As came up with the framework, Xcode keeps an auto-generated scheme for the target, until a change is made to the scheme that deviates from the defaults.

Make Xcode to create the app scheme file:

* ➡️ Product → Scheme -> Manage Schemes…
  - Uncheck, and then recheck, the “Shared” checkbox on the right side of the line for the app scheme.
  - Optionally, tap the name of the scheme in the list (“Tutorial”) and rename it for clarity (e.g., “TutorialApp”).
  - 👉 “Close”.

### ❇️ Commit Message

```
Make Xcode create the default file for the app scheme.

Xcode: 15.2
```

---

## Rename the Template App Swift File

If your *App Target Name* ends with “App”, the app target template in the previous section will have generated a Swift file ending with “…AppApp” (`TutorialAppApp.swift`), containing a `struct` of the same name.

Truncate the filename to just one “App”:

* In the Project Navigator (⌘-1), tap once on the name of the “…AppApp” file (normally the first file in the app group), to enter editing mode.
* Delete one “App” so it just ends with “App” instead of “AppApp”.
* Press the return key.

Truncate the `struct` name to just one “App”:

* With the app Swift file open in the editor…
* Rename the `struct` to end with just one `App`.
* While editing the file, you may also want to edit or remove the header comment at the top of the file.

Confirm that the app can build:

* ➡️ Product → Scheme → select the app scheme
* ➡️ Product → Build (⌘-B)

The build should succeed.

### ❇️ Commit Message

```
Change the TutorialAppApp to just TutorialApp.
```

---

## Cleanup App Template Code

Unfortunately, Apple’s templates will produce SwiftLint warnings.

* Open the “ContentView” file in the app (“Tutorial”) group.
* After the `Image(systemName: "globe")` line, insert a new line:
  - `.accessibilityHidden(true)`

That will tell the system’s accessibility support to ignore the globe icon (since it is just decorative).

### ❇️ Commit Message

```
Cleanup app template code to make SwiftLint happy.

Specifically, marked the globe image in `ContentView` as hidden for accessibility.
```

---

## Configure App Target

* In the Project Navigator (⌘-1), select the project at the top of the files.
* In the Project editor, select the app target.
  - General:
    - Minimum Deployments: Make sure these are the same as you set for the overall project settings.
    - Identity: App Category: Pick whatever is appropriate for your app.
    - Identity: Display Name: Set it to the *App Display Name* (if different from the app target name).
    - Identity: Version & Build: Change them as needed.
    - Deployment Info: Configure as appropriate to your app.
  - Build Settings: Select the App target ("Tutorial"). Ensure that:
    - Build Options: Enable Testability: Debug: Yes
    - Swift Compiler - Language: Enable Bare Slash Regex Literals: Yes
  - Build Settings: Select the project (blue icon at the top, above the targets):
    - Strict Concurrency Checking: Complete
      (It’s a “best practice” to always turn this on for new projects.)

Confirm that the app can still be built and run:

* ➡️ Product → Run (⌘-R)

### ❇️ Commit Message

```
Configure the app target.

* App Category: Developer Tools
* Display Name: Tutorial
* Version: 0.1
* Strict Concurrency Checking: Complete
```

---

## Add the framework as a dependency for the app

Add the framework to the app target:

* In the Project Navigator (⌘-1), select the project (top of list).
* Select the App target.
* Go to the General tab.
  - Under “Frameworks, Libraries, and Embedded Content”, 👉 the “+” button.
  - Select the framework and “Add” it.

(Because the framework is now a dependency for the app target, when the app target is built, the framework will be, too.)

### ❇️ Commit Message

```
Add the framework as a dependency for the app.
```

---

## Setup App Test Plan

Create the app test plan:

* ➡️ Product → Scheme → select the app scheme.
* ➡️ Product → Scheme → Edit Scheme… (⌘-< or ⌘-⇧-,)
* Select the Test section from the list on the left.
* 👉 the small arrow button to the right of the name of the “Autocreated” app test plan.
* ➡️ File → Save… (⌘-S)
  - The default name (“TutorialApp.xctestplan”) should be good, although you can choose something different.
  - Select the Tests directory.
  - Group: Tests

Set the position of the test plan in the project navigator:

* In the Project Navigator (⌘-1), move the test plan into the Tests group (if it isn’t already), after the framework test plan.

Add the framework unit tests to the app test plan:

* 👉 the “+” button at the bottom left of the edit test plan pane.
  - Select the framework tests target.
  - 👉 “Add”.
* 👉 “Options…”
  - Execute in parallel: on
* Select the Configurations tab.
* Make sure “Shared Settings” is selected.
* Test Execution: Execution Order: Random
* Code Coverage: Code Coverage: On
  - Gather coverage for: some targets
  - 👉 “+”, to add the framework and app targets.
  - Tap away from the pop-up selector to close it.

Confirm that the test plan works for the app scheme:

* ➡️ Product → Test (⌘-U)

The tests should succeed.

### ❇️ Commit Message

```
Add a test plan for the app.
```

---

Source: [Developer Docs](https://gitlab.com/grantneufeld/DeveloperDocs)
