# Setup to Test Everything

As a project grows, the time taken when running the full set of tests will grow. As [discussed earlier](4_Structure_of_Automated_Tests_in_an_Xcode_Project.md), it is useful to divide up tests into (at least) fast and slow. Fast tests can be run frequently while coding. The slower tests can be run less frequently, but should generally be run at least once before each commit.

A “TestEverything” scheme and test plan will be for running the full set(s) of tests for a project. The intention is that this be run prior to a commit, and perhaps as part of a continuous integration system.

The scheme doesn’t have a target of its own. It just collects all of the project’s test targets in a single test plan so that the complete suite(s) of project tests can be run together.

I have chosen to name the scheme, and corresponding test plan, “TestEverything”. The name has no impact on the project, so it can be called anything that is meaningful to the developers who will be using it.

---

## Add the TestEverything Scheme & Test Plan

Create the TestEverything scheme:

* ➡️ Product → Scheme → New Scheme…
  - Target: None
  - Name: “TestEverything”
* The scheme editor should be open, but if it isn’t:
  ➡️ Product → Scheme → Edit Scheme… (⌘-< or ⌘-⇧-,)
* Select the Build section in the left column.
* Use the “+” button in the bottom left to add the app and framework targets.
* Turn off the checkboxes for Run and Archive.

Create the TestEverything test plan:

* Select the Test section in the left column.
* 👉 the small arrow button on the right side of the TestEverything test plan.
* ➡️ File → Save (⌘-S)
  - Save As: TestEverything.xctestplan (should default to that)
  - Navigate to the “Tests” directory.
  - Group: Tests
  - 👉 “Save”.

* In the Project Navigator (⌘-1), move the TestEverything test plan to the end of the items in the Tests group.

Configure the test plan:

* In the “Tests” tab of the test plan editor, 👉 the “+” button at the bottom left of the pane.
  - Select all of the test targets, and 👉 “Add”.
* For each test target, 👉 “Options…”, and turn on Execute in parallel.
* Switch to the “Configurations” tab.
  - Select Shared Settings.
  - Test Execution: Execution Order: Random
  - Code Coverage: Code Coverage: On
    - Gather coverage for: some targets
    - 👉 “+”
      - Add the framework and app targets
    - Tap away from the pop-up selector to close it.

Make sure the build target is My Mac.

* ➡️ Product → Destination → My Mac

Run the tests:

* ➡️ Product → Test (⌘-U)

The tests will take a bit, but should pass at this point.

### ❇️ Commit Message

```
Add TestEverything scheme & test plan.

* Xcode: 15.2
```

---

Source: [Developer Docs](https://gitlab.com/grantneufeld/DeveloperDocs)
