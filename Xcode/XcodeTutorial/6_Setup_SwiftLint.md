# Setup SwiftLint

[SwiftLint](https://github.com/realm/SwiftLint) is “A tool to enforce Swift style and conventions.”

---

## Install SwiftLint

> ⚠️ Make sure you have the `swiftlint` command line tool installed, and available in your shell `PATH`.

You can use a tool like [Homebrew](https://brew.sh/) to install SwiftLint.

Otherwise, refer to the [SwiftLint](https://github.com/realm/SwiftLint) project for installation directions.

---

## SwiftLint is Incompatible with User Script Sandboxing

Because [SwiftLint doesn’t support “User Script Sandboxing”](https://github.com/realm/SwiftLint/issues/5053),
turn that option off for the framework target (which is the target we will be attaching SwiftLint to):

* In the Project Navigator (⌘-1), select the project (at the top of the list).
* Select the framework target.
* Switch to the Build Settings tab.
* Turn off the “User Script Sandboxing” setting (under Build Options).

### ❇️ Commit Message

```
Turn off User Script Sandboxing for the framework target.

This is because SwiftLint doesn’t work with that yet:
https://github.com/realm/SwiftLint/issues/5053
```

---

## Adding SwiftLint as a Run Script in the Project

Add a run script build phase for SwiftLint, right before compiling the framework:

* In the Project Navigator (⌘-1), select the project (at the top of the list).
* Select the framework target.
* Switch to the Build Phases tab.
* 👉 “+” at the top-left of the panel (not the one at the bottom-left).
  - New Run Script Phase.
* Drag the Run Script phase so it is above the Compile Sources phase.
* Double-tap on “Run Script” to enable editing of the name, and change it to “Run Script: SwiftLint”
* Expand the Run Script phase by tapping 👉 “>” to the left of “Run Script: SwiftLint”.
  - If your `swiftlint` binary is not in the default path
    (e.g., on Apple Silicon, Homebrew installs swiftlint in an unusual location),
    or your project is going to be used on multiple computers,
    you may want to change the Shell to something like `/bin/zsh` to get access
    to assigning `PATH` environment variable (such as in from `.zshrc`),
    since `/bin/sh` uses only the default system `PATH`.
  - Set the script to:

```sh
if which swiftlint >/dev/null; then
    swiftlint
else
    echo "warning: SwiftLint not installed"
fi
```

  - For install builds only: off
  - Based on dependency analysis: off
  - Show environment variables in build log: off (but usually doesn’t matter)
  - Use discovered dependency file: off

### Configuring SwiftLint

SwiftLint can be configured using `.swiftlint.yml` configuration files. The options from the file will apply to all files in the directory and sub-directories, but can be overridden in sub-directories by having another `.swiftlint.yml` in the sub-directory.

#### Add SwiftLint configuration files

*(🛑 TODO: add links to sample .swiftlint.yml files for the tutorial)*

* Add a `.swiftlint.yml` file into the root directory of your project.
* You may also want to add a different `.swiftlint.yml` file for your tests, to turn off some checks that don’t apply to test code.
  - Create a new group/directory at the top level of the project, and name it “Tests”
  - Add a `.swiftlint.yml` file into the `Tests` directory, with the appropriate rule overrides for test code.

### Verifying SwiftLint is Working

Confirm that it is working by making some code in the project deliberately messy, so that it should trigger warnings from SwiftLint:

* Back to the Project Navigator (⌘-1).
* Add a swift file in the framework group.
  - ➡️ File -> New -> File… (⌘-N)
    - macOS: Swift File
    - 👉 “Next”
    - Give it a name like `TryOutSwiftLint`
    - 👉 “Create”

Set the content of that new file to:

```swift
struct swiftlint_WontLikeThis {

var Stuff = 123456 //nope
}
```

* ➡️ Product → Build (⌘-B)

The Issue Navigator should now show a bunch of errors and warnings from SwiftLint.

Cleanup the content of `TryOutSwiftLint.swift` to make SwiftLint happy.

```swift
struct TryOutSwiftLint {
    var stuff = 123_456 // yep
}
```

* ➡️ Product → Build (⌘-B)

There should be no more errors or warnings reported.

Finally, delete the `TryOutSwiftLint.swift` file, moving it to the trash.

> Note that, until you add a `.swift` file (in the next section of this tutorial), running Build again will produce an error because SwiftLint returns an error when there are no Swift files for it to analyze.

### ❇️ Commit Message

```
Setup SwiftLint to run whenever the framework is built.

SwiftLint: 0.57.0
```

---

Source: [Developer Docs](https://gitlab.com/grantneufeld/DeveloperDocs)
