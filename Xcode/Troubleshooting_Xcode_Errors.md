# Troubleshooting Xcode Errors

Xcode is not known for being the most stable of development tools. You will occasionally run into errors or unexpected behaviours while trying to work with it.

Often, the errors are in our code. But, sometimes, it can be a bug on Xcode’s part.

After trying to find the problem in your code—including searching the web for answers—if it seems that Xcode is at fault, the following are some things you can do, in increasing order of being drastic, to try to get it back on track.

After each of these steps, reopen your project, if necessary, and try to build/test again to see if the problem persists.

## 1: Clean the Build Folder

* ➡️ Product -> Clean Build Folder…
   
This is the first thing to try, and you will likely benefit from learning the keyboard shortcut as it can be helpful to use it often (⌘-⇧-K by default).

## 2: Delete the App from Simulator

If working with Simulator, delete the app in the simulator to clear possibly outdated data and configuration.

## 2: Quit and relaunch Xcode

“Have you tried turning it off and on again.”

## 3: Delete the project’s “DerivedData”

DerivedData is hidden away inside the Library in your user directory:

    ~/Library/Developer/Xcode/DerivedData

Inside that directory, are directories for each project that you have ever loaded with Xcode on that machine (sometimes more than one directory for a given project). They will have names that start with the project name, followed by a dash and a long string of seemingly random characters.

Make sure Xcode is not running, then trash the derived data directory for the project (not the whole DerivedData directory, at this stage).

## 4: Shutdown and restart the computer

Not too likely to help, but you’re probably getting desperate at this point.

## 5: Eradicate Xcode’s entire “DerivedData”

If none of the above worked, go back to Xcode’s DerivedData and, after making sure that Xcode is not running trash the whole directory.

    ~/Library/Developer/Xcode/DerivedData

## 6: Rollback to an earlier commit in source control

Try rolling back, in source control, to an earlier commit that was working.

## 7: Try it on a different computer

If, in the previous step, the older, previously working, version is now experiencing the same error, try using the project on a different computer to see if the same error occurs.

If the error persists, it’s a problem in your project.

If the error does not occur on the other computer, it’s probably one or more of:

* Your Xcode installation is damaged, or misconfigured.
* Something in your user account settings is causing problems
  (things like your shell configuration files, or maybe something in your `~/Library/`).
* An issue with the system software
  (you could take the very desperate measure of wiping and reinstalling your system).
* There’s a hardware problem with your computer system.

## 8: Desperate Measures

These are things to try only after exhausting everything above, and only if you’re *really* desperate…

* Recreate the project from scratch, copying over source files one-by-one.
  (Note that the old version control history will not be available in that project.
  So, you may want to keep the old repository around for reference.)
* Delete and reinstall Xcode.
* Wipe and reinstall your system.
  This is the most drastic of measures and is not to be undertaken lightly
  as it can entail a ton of work. Be absolutely certain before taking this step,
  and be sure to have at least two full backups on separate drives
  (possibly including a remote backup, which is good to have anyway).
* Replace your computer.

## 9: Give up on Computer Programming?

Maybe go and become a farmer, instead? You certainly wouldn’t be the first of us to reach the level of frustration to do that 🤬😂🫥

---

Source: [Developer Docs](https://gitlab.com/grantneufeld/DeveloperDocs)
