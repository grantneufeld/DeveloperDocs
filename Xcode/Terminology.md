# Xcode Project Terminology

## Commit

A commit is the act of adding revisions to the *files* of a project to the *source control* repository for the project.

## Destination

The destination determines the platform to currently compile/build/run/test for. It can be a device (such as “My Mac” for the computer Xcode is running on), a simulator (such as an iOS simulator), or a “Build” destination to compile for “any” destination by architecture.

You can set and manage destinations through the Product menu’s Destination sub-menu, or by tapping the destination at the top of the Xcode Project window (immediately to the right of the scheme).

## File

Source code, data, assets, etc.

Files can be referenced in the top of the project (project’s root directory), but they will normally (except for some documentation files, like a README), be contained in a *group* (directory) within the project.

A file may be assigned “membership” in any number of *targets* for the project, including zero (e.g., a README text document is just reference for the project, and does not get compiled into any target).

Files are accessed and managed through the Project Navigator pane.

* ➡️ View → Navigator → Project (⌘-1)

## Group

*Usually* corresponds with a sub-folder/directory in the *project directory*.

Some groups map to a *target* in the project. It’s generally best to avoid renaming those target groups/directories, as the name/path typically gets used in multiple places inside Xcode’s settings for the project, and can be a hassle to track down and update.

Groups are accessed and managed through the Project Navigator pane .

* ➡️ View → Navigator → Project (⌘-1)

## Project Directory/Folder

The “root” of a project. This will be the directory that contains the project file, and any other *files* that make up the project.

Any files or directories not contained in this directory are generally not part of the project.

## Project File

The `.xcodeproj` file for a project.

It is actually a directory containing various configuration files for the project, but is treated as a file on macOS because it is marked as a “package” (which is macOS’ way of flagging a directory to be presented to the user as if it were a file).

## Scheme

A scheme ties together *targets* and *test plans*. It can have any number of targets to be built, which test plan(s) (if any) to run, and other configuration options for how the targets and built products can be run, profiled, analyzed, and/or archived.

Targets and test plans are often “set once and leave it”—we usually don’t interact with them directly much. But schemes, if you have more than one, you may find yourself frequently switching between them. (Such as when you have a scheme with just the fast tests for use while doing TDD, and another one with slow tests for use before a commit.)

Schemes are accessed and managed through the Product menu’s Scheme sub-menu, or by clicking the scheme at the top of the Xcode project window.

## Source Control

(Often called “version control”.)

This is the mechanism by which a “source repository” for a project is maintained.

Maintaining a good source repository (through frequent *commits*) allows for tracking the stages of development of a project, as well as the important ability to “rollback” a project to an earlier version (when a serious problem with the code is found, or in other situations).

## Target

This defines a distinct thing that Xcode will build (app, framework, test suite, extension, etc.).

You can view and modify a project’s targets by selecting the project at the top of the Project Navigator pane.

* ➡️ View → Navigator → Project (⌘-1)
* Select the project at the top of the list.

There’s a sidebar on the left of the project editor pane (if the sidebar is hidden, tap the button it the near the top left of the pane to show it). The sidebar lists the project at the top, followed by a section listing the targets in the project.

Each target has numerous configuration options that can vary depending on the type of target.

A target can depend on the output of another target (such a tests target that depends on the product of an app target to test, or when a target depends on a framework target).

## Test Plan

A test plan groups any number of test *targets*, and sets configuration options for how they will be run.

The same test target can be included in any number of test plans in the project.

Test plan files can be accessed through the Project Navigator pane.

* ➡️ View → Navigator → Project (⌘-1)

They can also be accessed through the Test section in the scheme editor.

---

Source: [Developer Docs](https://gitlab.com/grantneufeld/DeveloperDocs)
