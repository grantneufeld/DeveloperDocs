# Add a Command Line Tool to a macOS App Project

## Add Command Line Tool Target

To add a command line tool as a target to an existing macOS app project in Xcode:

1. Make sure the project is open in Xcode, and is frontmost (active).
2. Show the Project Navigator (⌘-1).
3. Select the project itself (the top item in the file list). This will bring up the “project configuration” view, with various tabs like “General”, “Signing & Capabilities”, etc.
4. Make sure the “project and targets list” is visible (it’s a thin column immediately to the right of the Navigator column). If it’s not showing, there’s a small square button near the top left to show/hide that column.
5. In the bottom-left corner of the project and targets list is a small plus (“+”) button. Click that to bring up the target template chooser.
6. The chooser has a number of tabs. Select “macOS”.
7. From the list of choices, select “Command Line Tool” (usually found about 3 sections down, under “Application”; or you can type “com” into the filter box).
8. Click the “Next” button (bottom-right).
9. Fill in the options form as usual, but give special consideration to the Product Name (see the section following this list for naming considerations). Also make sure that the “Project:” is the correct one you want to add to.
10. Click the “Finish” button (bottom-right).

### Naming Your Command Line Tool Target

When deciding on a name for your command line tool target:

- If there is a possibility of having more than one command line tool for the project, use the tool’s name in the target name.
- If you are certain that there is only ever going to be one command line tool target in the project, you might want to name it something like “CommandLineTool” or “MyAppCLI”.
- In general, you’ll want your targets to have names that reflect their product (“...App”, “...UITests”, “...CLI”, “...CLInterfaceTests”).

## Configure Command Line Tool Target

With the Command Line Tool target selected, select the “Build Settings” tab.

Some options you may want to adjust:

- Build Settings -> Packaging -> Product Name: The name you want for the executable binary  (this is how the tool will be invoked from the command line). You will generally want a fairly short name, all in lower-case letters.
- Deployment: “Installation Directory”
- ...?

## Using the swift-argument-parser library

### Install the package

Xcode -> File -> Add Package Dependencies

In the Search field, enter:

    https://github.com/apple/swift-argument-parser.git

When the `swift-argument-parser` is shown, select it.

Set the “Dependency Rule” as you like (the defaults are usually good).

For “Add to Project”, make sure the project you want is selected.

Then click the “Add Package” button.

Add the “ArgumentParser” library to the command line tool target (but not the other targets in the project)

### Setting up the main entry point

If you had Xcode add a command line tool target to the project, it will have created a `main.swift` file. To use the `swift-argument-parser` package, either rename or delete that file and create a new one. The name mostly doesn’t matter (just not `main`).

Import the `ArgumentParser` module (from the package).

```swift
import ArgumentParser
```

For the entry point, there will need to be a `struct` marked with `@main`, and conforming to the `ParsableCommand` protocol.

```swift
@main
struct MyCommand: ParsableCommand {
}
```

That needs a `mutating func` named `run` that `throws`:

```swift
mutating func run() throws {
  // perform the command’s function(s)
}
```

### Command Line Arguments (Variables)

This is the part where one should really read through the [ArgumentParser documentation](https://swiftpackageindex.com/apple/swift-argument-parser/documentation/argumentparser).

But, for quick reference, …

- `@Argument`: positional input (the order of arguments in the struct determines the order the values have to be provided on the command line).
- `@Option`: labeled input (the value is preceded by a `--name` label on the command line).
- `@Flag`: used for boolean values (no value is included with the flag, just the flag itself)

#### Argument names/labels

Variable names from the struct are converted from `camelCaseName` to `hyphenated-name` format when used for the command line parameters.

The parameter label can also use a short form by passing `.shortAndLong` to the property wrapper’s `name` parameter. For example, `@Option(name: .shortAndLong) var foo: String` would allow both `--foo` and `-f` to be used.

The parameter name can be overridden by passing a `.customLong()` value to the property wrapper’s `name` parameter. For example, `@Option(name: .customLong("foo")) var bar: String` would use `--foo` on the command line, instead of using the struct’s variable name `--bar`.

The parameter’s short name defaults to the first character of the variable name. It can be overridden by passing a `.customShort()` value (a single character) to the property wrapper’s `name` parameter. For example, `@Option(name: .customShort("z")) var foo: String` would use `-z` on the command line, instead of `-f`.

Multiple options can be combined in an array. E.g., `@Option(name: [.short, .customLong("bar")])`.

#### Providing Help

Descriptive help text can be provided as a parameter passed to a variable’s property wrapper.

```swift
@Option(help: "This explains what the argument is for…")
var myArgument: String
```

That can be combined with the custom name parameter:

```swift
@Option(name: .shortAndLong, help: "This explains what the argument is for…")
var foo: String
```

---

Source: [Developer Docs](https://gitlab.com/grantneufeld/DeveloperDocs)
