# Testing a Command Line Tool in Xcode

> **DRAFT**
> This document is posted for review and may not yet be fit for use.

## Unit Tests for a Command Line Tool

Unit tests should generally still be handled as normal.

### Shared Code

If the Command Line Tool target shares code, such as with an app target,
it is generally optimal to separate most code from the Command Line Tool target
— typically through a shared Framework or Package.

With that setup, almost all unit tests will apply to the framework/package.
That leaves just the command line interface tests
(the command line equivalent of UI tests).

## Interface Tests for a Command Line Tool

What you want to test here is that the various arguments to the command line
(passed in filepaths, flags, etc.) produce the expected results.

This is not an exhaustive testing of the full system — just the handling of arguments
(the interface).

Testing the command line interface should be handled in a separate set of tests.
There should be a separate target, and perhaps a separate test plan and/or scheme, for those tests.

### Add a Target for Command Line Tool Interface Tests

- When looking at the project (click the project — top item in the Navigator),
  add a Target.
- I went with a “macOS” -> “**UI Testing Bundle**” as the target template type.
- Product Name: If you will only have one command line tool being built as part of your project, something like “`CommandLineInterfaceTests`” may be suitable. In any case, the name should reflect that it is for the command line tool and is for interface tests.
- Target to be Tested: **None**. Xcode does not let you select a Command Line Tool target
  here (same limitation for Unit Testing Bundle targets, too).

### Add a Scheme & Test Plan

#### Add the Scheme

If you don’t already have a scheme for your command line tool target, add one:

- Menu: Product -> Scheme -> New Scheme…
- Target: CommandLineTool (or whatever name you used)
- Name: (usually the same as the target)

### Configure the Test Plan

Add the Command Line Interface Targets to the test plans for the Command Line Tool scheme.

- Make sure the scheme is selected.
- Menu: Product -> Scheme -> Edit Scheme…
- Select the “Test” section from the sidebar on the left.

There may be one (auto-created) item in the list of “Test Plans”, that refers to the command line tool target.
Generally, that should be fine as-is. But if you want/need to do something different
with it, you can click on the small arrow (pointing right) in a circle to modify
a test plan in the list, or click the “+” at the bottom left of the list to add another
test plan to the scheme.

- Make sure the “Test” section is selected on the left side of the window.
- “+” button at the bottom-left of the test plans list.
- “Create empty Test Plan…”
- Save As: Choose a name for the test plan that either matches your Command Line Interface Tests target name, or the Command Line Tool target name.
- Group: Select the Command Line Interface Tests, or the Command Line Tool, group.

Setup the new test plan:

- Click the small button (right-pointing arrow in a circle) beside the name of the test plan to bring up the test plan in the editor.
- Make sure the “Tests” (not “Configurations”) tab is selected near the top middle of the editor.
- Click the “+” button near the bottom left of the editor.
- Select the Command Line Interface Tests target, and click “Add”.
- Click the “Options…” button, and configure as you prefer (I usually use “Execute in parallel” and “Automatically include new tests”).
- Switch to the “Configurations” tab.
- Select “Shared Settings” on the left.
- Arguments -> Target for Variable Expansion -> select the Command Line Tool target.
- Test Execution -> Execution Order -> I prefer “Random”.

🛑 INCOMPLETE

Now, when the command line tool scheme is active, you should be able to run the interface tests (including by using ⌘-U).

### Making the Compiled Binary Available to Your Tests

Xcode builds the executable binary deep inside the obscure DerivedData hierarchy.

To make it accessible, I like to symlink it inside a `Products` directory
at the top level of the project directory.

To automate this, I add a “Run Script” action to the end of the “Build Phases” for the command line tool target.

- Shell: `/bin/sh`
- Run script:
  - For install builds only: off
  - Based on dependency analysis: off
- Show environment variables in build log: off
- Use discovered dependency file: off
- Output files:
  - `$(SRCROOT)/Products/$(FULL_PRODUCT_NAME)`

With the script:

```sh
if [ ! -d Products ]; then
  echo "warning: creating Products directory in project folder"
  mkdir Products
fi
if [ ! -e "Products/$FULL_PRODUCT_NAME" ]; then
  echo "warning: linking built tool to Products directory"
  ln -s "$BUILT_PRODUCTS_DIR/$FULL_PRODUCT_NAME" Products
fi
```

Currently, that script only 

### Writing Interface Tests for a Command Line Tool

> **NOTE**
> This type of test can be used for any command line tool
> — not just ones built with Xcode, or written in Swift.

#### Test Helpers

I find it useful to have some helper methods for the tests to handle calling the
command and getting the data from `stdout`. (It wouldn’t take much to also capture
`stderr` — just setup a `Pipe` for it, like for `.standardError` on the `Process`.)

First, a property to store the name of the command:

```swift
/// The filename of the executable binary for the command line tool.
var filename = "mycommand"
```

Then a computed property for the URL to the command’s executable binary on the local
filesystem.

For this, I’m assuming the command is in a `Products` directory at the top level of
the project folder, and that the test file with the helpers is in a directory that is
also at the top level of the project folder. If either of those are in different
locations, this code would have to be adjusted accordingly.

```swift
/// The local filesystem URL for the executable command line tool binary.
private var commandURL: URL {
    let currentFile = URL(filePath: #filePath)
    let testFolder = currentFile.deletingLastPathComponent()
    let projectFolder = testFolder.deletingLastPathComponent()
    let productsFolder = projectFolder.appending(path: "Products")
    return productsFolder.appending(path: self.filename)
}
```

A computed property for getting the full filepath to the executable binary.

```swift
/// The filepath of the executable command line tool binary, in the local filesystem.
private var commandPath: String {
    return self.commandURL.relativePath
}
```

An error that can be thrown if the executable binary is not where expected.

```swift
enum CommandLineInterfaceTestsError: Error {
case missingExecutableBinary
}
```

Finally, the function used to actually perform a command line tool call,
and get the resulting output.

```swift
/// Call the executable command line tool binary.
/// - Parameter arguments: An array of strings for the arguments to pass
/// - to the command line tool.
/// - Returns: A `String` of anything the command outputs to `stdout`.
///   (The text returned from the command.)
/// - Throws: `CommandLineInterfaceTestsError.missingExecutableBinary`
///   if the binary is missing.
///   Any `Process` errors when trying to run the command.
private func call(_ arguments: [String]) throws -> String {
    guard FileManager.default.fileExists(atPath: self.commandPath) else {
        throw CommandLineInterfaceTestsError.missingExecutableBinary
    }

    var result = ""

    let captureOut = Pipe()
    captureOut.fileHandleForReading.readabilityHandler = { pipe in
        if let line = String(data: pipe.availableData, encoding: .utf8) {
            result.append(line)
        }
    }

    let process = Process()
    process.executableURL = self.commandURL
    process.arguments = arguments
    process.standardOutput = captureOut
    try process.run()
    process.waitUntilExit()

    return result
}
```

#### Example Interface Test

With the above helpers included in the test file, tests become pretty simple.

For example: Assuming a command called `quote`, which takes a flag `--single`,
and an arbitrary text argument, we might have a test like:

```swift
func test_quote_givenSingle_returnsSingleQuotedText() throws {
    let result = try self.call(["--single", "test"])
    let expected = "'test'\n"
    XCTAssertEqual(result, expected)
}
```

Note the trailing newline (`\n`) at the end of the `expected` string. There will
typically be a newline break at the end of the output from a command line tool.

#### Example Interface Test File Structure

You might have a test file named something like:
`MyProject/CommandLineTests/CommandLineToolTests.swift`

With the code:

```swift
import XCTest

/// WARNING: This requires that the built command line tool be available in the
///   `Products` sub-directory of the project directory.
final class CommandLineInterfaceTests: XCTestCase {
    // ... test functions ...

    /// MARK: - Helpers

    // ... Insert the helpers from above here ...
}
```

---

Source: [Developer Docs](https://gitlab.com/grantneufeld/DeveloperDocs)
