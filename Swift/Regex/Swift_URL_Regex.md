# A Swift Regular Expression for URLs

This documents a regular expression which can find URLs, and map the individual components. This explanation is intended to be usable by people not familiar with regex syntax (so the experienced folks can probably skip over most of the stuff here).

## Contents

- The URL Regular Expression (regex).
- Quick Regex Syntax Summary (for those not familiar with regular expressions).
- Detailed breakdown of the URL regex (explaining each part in the context of the syntax for URLs).
- Example Usage of Regex in Swift.

## The URL Regular Expression (regex)

_(Note that this is not 100% strict. I take some shortcuts. It should not reject any valid URLs, but it may match against some strings that are not truly valid URLs, but you will be very unlikely to encounter those. If you want 100% strict validation, you will need to add a networking (not regex) layer of validation of domain-names / IP addresses / IPv6 addresses.)_

Here’s the big ugly regex:

```swift
let urlRegex =
/(?<protocol>https?):\/\/(?:(?<username>[^:@\s\/\\]*)(?::(?<password>[^:@\s\/\\]*))?@)?(?<domain>[\w\d]+[\w\d.\-]+[\w\d]+|\[[a-f\d:]+\])(?::(?<port>\d+))?(?:(?<path>\/[^\?#\s]*)(?:\?(?<query>[^\?#\s]*))?(?:#(?<anchor>[^\?#\s]*))?)?/
```

And broken into lines for readability (same as above, but with linebreaks and spaces added.
<br>**So, don’t use this one**:

```swift
/
(?<protocol>https?):\/\/
(?:
  (?<username>[^:@\s\/\\]*)
  (?::(?<password>[^:@\s\/\\]*))?
  @
)?
(?<domain>
  [\w\d]+[\w\d.\-]+[\w\d]+
|
  \[[a-f\d:]+\]
)
(?:
  :(?<port>\d+)
)?
(?:
  (?<path>\/[^\?#\s]*
)
(?:
  \?(?<query>[^\?#\s]*)
)?
(?:
  #(?<anchor>[^\?#\s]*))?
)?
/
```

---

## Quick (incomplete) Regex Syntax Summary

_(This is just enough regex explanation to cover what is in the URL regex.)_

Swift Regex expressions start and end with a slash `/`. If you need to include a slash in the content of the regex, precede it with a backslash `\` (the “escape character”) as in `/This has a slash "\/" in quotes./`.

That escape character, the backslash `\`, shows up a lot. It comes before another character and the combination of the two defines an “escape sequence” for special behaviours. If you need an actual backslash character in the regex, you can use a double-backslash `\\`.

The period character `.` specifies any character, except line-breaks/return. E.g., `d.ve` would match `dave`, `d_ve`, `d ve`, `d3ve`, etc.

The `\w` sequence specifies any word-character (so not whitespace, punctuation, or numbers).

The `\d` sequence specifies a numeric digit.

The `\s` sequence specifies a white-space character (space, tab, linebreak, carriage-return).

There are 4 ways to define how many occurrences of a character (or other chunk) there should be:

- `?` specifies zero or one occurrence. E.g., `cats?` would match `cat` or `cats`.
- `*` specifies any number of repetitions (zero or more). E.g., `do*g` would match `dg`, `dog`, `dooooooog`, etc.
- `+` specifies one or more occurrences. E.g., `bir+d` would match `bird`, `birrrrrd`, etc.
- The fourth way is to use squiggly brackets (`{` and `}`) with numeric values:
  - A single number specifies an exact count. E.g., `slo{3}th` would match `sloooth`.
  - A single number followed by a comma specifies a minimum count. E.g., `ra{2,}t` would match `raat`, `raaaaaat`, etc.
  - A comma followed by a single number specifies a maximum count. E.g., `bu{,2}g` would match `bg`, `bug`, and `buug`.
  - A number, followed by a comma, followed by another number, specifies a minimum and a maximum. E.g., `fro{1,3}g` would match `frog`, `froog`, and `frooog`. 

Square brackets define a collection of possible characters. E.g., if you wanted to search for vowels, you could use `[aeiou]`.

Square brackets where the first character inside the brackets is a caret `^` specify to look for any character _except_ the specified characters. E.g., to find any character _except_ a vowel, you could use `[^aeiou]`.

The bar `|` character denotes alternatives. E.g., The regex `/A big (cat|dog)!/` would match `"A big cat!"` and `"A big dog!"`

Round brackets define a “capture group” within the regex, that can then be referenced from the result. E.g., a regex like `/a(b)c/` would find the sequence of characters `abc`, and have one capture group which would consist of `b`. Capture groups automatically have an index number (starting at 1) based on their order in the regex.

Capture groups can be named by starting (right after the opening `(` bracket) with a question mark `?` followed by the name in angle brackets. E.g., `a(?<example>b)c` would define a capture group named `example` which would find `b` between `a` and `c`; and accessing the value of the example capture group after a successful search would return `b`.

To define a group that will not be captured, open the group with `(?:`. E.g., `/The (?:yellow )?bird flew./` won’t have any capture groups, and will match `"The bird flew."` and `"The yellow bird flew."`.

---

## Breaking down the URL regex

Since there’s a lot going on in it, and it’s pretty much unreadable by humans, here’s a part-by-part breakdown of the regex.

### Protocol

```swift
/(?<protocol>https?):\/\//
```

URLs start with a protocol, followed by a colon (`:`). Many, but not all, types of URLs then have two slashes (`//`). (Those slashes are a historical legacy that doesn’t actually add anything, but we’re stuck with it.)

This part of the regex defines a named capture group (“`protocol`”) which will either be `http` or `https`.

The regex is set to only handle http and https URLs. If you want to support other protocols, change this part of the regex. E.g., `(?<protocol>[a-z]+(?:[a-z\-]*[a-z+]+)?):(?:\/\/)?` would let you find any URL (that follows proper URL format), with a protocol name consisting of characters from “a” to “z”, optionally including hyphens (but not at the start or end). It would als make the `//` part of the URL optional, because not all URLs require that (e.g., `mailto:some@email.address`).

### Host Access (domain, port, username, password)

While the vast majority of URLs refer to the host by just the domain name (e.g., `http://www.domain.tld/`), the URL specification allows for some variations:

- An IP Address can be used instead of a domain name. E.g., `http://192.168.0.1/`.
- An IPv6 Address can be used instead of a domain name. E.g., `http://[123:456:::7890]/`.
- The “localhost” can be specified instead of a domain name. E.g., `http://localhost/`.
- A “port” can be specified. (Web/HTTP protocol services are normally on port 80, but alternate ports can be used.) E.g., `http://example.tld:8080` would specify connecting to port 8080 on the example.tld server.
- A username and (optional) password can be specified (although this can be a significant security risk if used). E.g., `http://my_username:secret-pass@example.tld/` provides “`my_username`” as the username, and “`secret-pass`” as the password. (The colon `:` and password can be omitted if just the username is to be supplied. E.g., `http://username@example.tld/`.)

#### username & password

```swift
/(?:(?<username>[^:@\s\/\\]*)(?::(?<password>[^:@\s\/\\]*))?@)?/
```

This whole part may or may not be present (normally not).

It defines a `username` which is a sequence of characters that are not a colon `:`, at-symbol `@`, white-space, slash `/`, or backslash `\`.

If there is a password part, it is separated from the username by a colon `:`. The `password` is a sequence of characters just like the username (not `:`, `@`, white-space, slash, or backslash).

If there is a username and/or password, there will be an `@` after them—separating them from the domain part of the URL which immediately follows.

_(A reminder that including this information, especially the password, in a URL, is a security risk.)_

#### domain

```swift
/(?<domain>[\w\d]+[\w\d.\-]+[\w\d]+|\[[a-f\d:]+\])/
```

There are two different ways for the `domain` to be identified (separated in the regexp by a bar `|`):

- `[\w\d]+[\w\d.\-]+[\w\d]+`
  - It starts and ends with letters from “a” to “z” and/or numeric digits. In the middle, it may have any number of those letters and digits, along with hyphens and periods. This is for either a domain name, or an IPv4 address.
- `\[[a-f\d:]+\]`
  - It starts with an open square bracket `[`, and ends with a close square bracket `]`. In between, there are any number of hexadecimal digits (0-9 and a-f), along with colons `:`, to form an IPv6 address.

#### port

```swift
/(?::(?<port>\d+))?/
```

If there is a `port`, it is immediately after the domain—separated by a colon `:`. The port is just one or more numeric digits.

### Path

```swift
/(?:(?<path>\/[^\?#\s]*)/
```

The `path`, if present, will start with a slash `/`. That is then followed by any number of characters that are not question marks `?`, pound/hash symbols `#`, or white-space (space, tab, linefeed, return).

### Query

```swift
/(?:\?(?<query>[^\?#\s]*))?/
```

The `query`, if present, will start with a question mark `?`. Like the path, that is then followed by any number of characters that are not question marks `?`, pound/hash symbols `#`, or white-space.

However, queries normally take one of two forms:

- A simple word or sequence of words (often separated with spaces encode with %-hex encoding “`%20`” or a plus symbol “`+`”).
- A collection of one or more key-value pairs where the keys (labels) and values are separated by the equals sign `=`, and the pairs are separated by ampersands `&`.

### Anchors / Fragments

```swift
/(?:#(?<anchor>[^\?#\s]*))?)?/
```

At the end of an URL, there can be an anchor (aka: “sub-link”; official name “fragment”). The `anchor` is separated from the rest of the URL that precedes it by a pound/hash symbol `#`.

Just like with the path and query, that is then followed by any number of characters that are not question marks `?`, pound/hash symbols `#`, or white-space.

---

## Example Usage of Regex in Swift

Using the `urlRegex` from the top of this document (`/.../` as a placeholder below for readability), here’s an example pulling the various parts from the URL string and checking them using XCTest’s `XCTAssertEqual` function:

```swift
let urlRegex = /.../

func test_urlRegex_findsAllTheParts() throws {
  let myURL = "http://me:secret@my.domain.tld:123/dir/doc.html?k=my+find#a42"
  let urlParts = try urlRegex.wholeMatch(in: myURL)
  XCTAssertEqual(urlParts?.output.protocol, "http")
  XCTAssertEqual(urlParts?.output.username, "me")
  XCTAssertEqual(urlParts?.output.password, "secret")
  XCTAssertEqual(urlParts?.output.domain, "my.domain.tld")
  XCTAssertEqual(urlParts?.output.port, "123")
  XCTAssertEqual(urlParts?.output.path, "/dir/doc.html")
  XCTAssertEqual(urlParts?.output.query, "k=my+find")
  XCTAssertEqual(urlParts?.output.anchor, "a42")
}
```

And how you might check to see if a given string is a valid URL:

```swift
let urlRegex = /.../

func validURL(_ url: String) -> Bool {
  do {
    let match = try urlRegex.wholeMatch(in: url)
    return match != nil
  } catch {
    return false
  }
}
```

---

Source: [Developer Docs](https://gitlab.com/grantneufeld/DeveloperDocs)
