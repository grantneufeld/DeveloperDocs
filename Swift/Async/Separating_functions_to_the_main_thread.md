# Separating functions that require the main thread in Swift

This is a very simple pattern in Swift for separating calls that have to be done on the main thread from any other calls that don’t care which thread they are on.

Particularly relevant when working with properties that are part of an `ObservableObject` in SwiftUI, since those impact view rendering so must only be modified on the main thread.

```swift
class MyExampleClass: ObservableObject {
    // Using the `@MainActor` tag on a variable like this will cause warnings to be
    // issued if it’s ever assigned to on a non-main thread.
    // (Thanks to Rad'Val on the iOS Developers Slack for the tip)
    @MainActor @Published var publishedValue: MyValueType?

    // Function that can be called on any thread.
    // For parts of this that have to be done on the main thread,
    // call separate functions that are tagged with `@MainActor`.
    func doTheThing() async {
        // ... do a pile of complicated stuff that doesn’t care which
        // thread it is on
        let value = await someComplicatedThing()
        await assignValueOnMain(value)
        // ... do more stuff that doesn’t care about which thread it is on
    }

    // Minimal function that is solely for assigning values that should
    // only be changed on the main thread.
    @MainActor
    private func assignValueOnMain(_ value: MyValueType) async {
        self.publishedValue = value
    }
}
```

---

Source: [Developer Docs](https://gitlab.com/grantneufeld/DeveloperDocs)
