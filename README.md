# Developer Docs

A repository of documentation, guides, help, tips, etc.,
of relevance to software development.

Home: [Developer Docs](https://gitlab.com/grantneufeld/DeveloperDocs)

## Contents

- [Using Git for Version Control / Source Control](Git/)
  - [Including Multiple Contributors for a Single Commit](Git/Multiple_Contributors_to_a_Commit.md)
  - [Setup Remote Git Repository Using Xcode](Git/Setup_Remote_Git_Repository_Using_Xcode.md)

- Swift
  - Async
    - [Separating functions that require the main thread in Swift](Swift/Async/Separating_functions_to_the_main_thread.md)
  - CommandLine
    - [Add a Command Line Tool to a macOS App Project](Swift/CommandLine/Add_command_line_tool_to_macOS_app_project.md)
    - [Testing a Command Line Tool in Xcode](Testing_command_line_tool.md)
  - Regex
    - [A Swift Regular Expression for URLs](Swift/Regex/Swift_URL_Regex.md)

- Xcode
  - [Xcode Project Terminology](Xcode/Terminology.md)
  - [Troubleshooting Xcode Errors](Xcode/Troubleshooting_Xcode_Errors.md)
  - [Xcode Project Structure Tutorial](Xcode/XcodeTutorial/)

## Author

Written by [Grant Neufeld](https://gitlab.com/grantneufeld).
(Social media and other links on [Linktree](https://linktr.ee/grantzero).)

Copyright ©2023-2024 Grant Neufeld. All Rights Reserved.
